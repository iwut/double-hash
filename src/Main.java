import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Random;

public class Main {

	String fileString = "input1";
	String hashMatrixFile = "hashMatrix";
	String outFile = "output";
	BitSet[] bitSet;
	int[] hashed;
	int total;
	Random random;
	boolean firstHashMade = false;
	Kattio io;

	@SuppressWarnings("unchecked")
	ArrayList<String>[] stringArr = new ArrayList[4194304];

	public Main() throws IOException {
		hashed = new int[4194304]; // 2^22
		makeHashMatrix();
//		writer.close();
		hashStuff();

		firstHashMade = true;
		doubleHashStuff();

	}

	/**
	 * Used to hash words that collided in the first hash function
	 */
	private void doubleHashStuff() {
		FileOutputStream outputStream = null;
		InputStream inputStream = null;
		try {
			outputStream = new FileOutputStream(new File(outFile));
			inputStream = System.in;
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

		io = new Kattio(inputStream, outputStream);
		
		io.println();

		random = new Random();
		int size;
		ArrayList<String> tmpArr;
		BitSet[] tmpBitSet;
		int[] checkArr;
		BitSet tmpSet;

		int result;
		outerloop: for (int i = 0; i < stringArr.length; i++) {
			if (hashed[i] > 2) {
				// result = 0;
				tmpArr = stringArr[i];
				size = (int) Math.ceil(Math.log(hashed[i]) / Math.log(2)); // log2
				checkArr = new int[(int) Math.pow(2, size)];

				tmpBitSet = makeRandomMatrix(size);

				// BitSet bitString = (BitSet) tmpSet.clone();

				int tot;
				BitSet resultSet = new BitSet();
				// hash that shit

				for (int j = 0; j < tmpArr.size(); j++) {

					result = hash(tmpArr.get(j), tmpBitSet);

					checkArr[result]++;

				}
				System.out.println();
				for (int j = 0; j < checkArr.length; j++) {
					if (checkArr[j] > 1) {
						i--;
						continue outerloop;
					}
				}
				
				// print if matrix is ok
				io.println("i: " + i + " #bits: " + size);
				
				printMatrix(tmpBitSet);
			}
		}
		
		
		io.close();

	}
	
	
	private void printMatrix(BitSet[] bitSet) {
		long tmp;
		String hex;
		for (int i = 0; i < bitSet.length; i++) {
			tmp = convertBitSetToLong(bitSet[i]);
			hex = Long.toHexString(tmp);
			
			io.println(hex);
		}
		io.println();
	}

	/**
	 * Make a randome Hi matrix
	 * @param size
	 * @return
	 */
	private BitSet[] makeRandomMatrix(int size) {
		BitSet[] ret = new BitSet[size];

		long rand;
		String bits;

		for (int i = 0; i < size; i++) {
			rand = random.nextLong();
			bits = Long.toBinaryString(rand);
			ret[i] = new BitSet();

			for (int j = 0; j < bits.length(); j++) {
				int bit = Integer.parseInt(bits.substring(j, j + 1));

				boolean bool = bit == 1;
				ret[i].set(j, bool);
			}

		}

		return ret;
	}

	/**
	 * Used to perform the first hashing
	 */
	private void hashStuff() {
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(fileString));
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

		Kattio io = new Kattio(inputStream, System.out);

		String bits;
		BitSet tmpSet;
		BitSet bitString;
		while (io.hasMoreTokens()) {
			hashed[hash(io.getWord(), bitSet)]++;
		}

		// io.close();

		int tmp = 0;
		int total = 0;

		for (int i = 0; i < hashed.length; i++) {
			tmp = hashed[i];
			if (tmp > 2) {
				total++;
			}
		}

		io.print(total);
		this.total = total;
		// System.out.println(total);
		io.close();

	}

	/**
	 * Hash function, iterate through bitSet matrix to perform bitwise and per row in matrix with hexword.
	 * @param hexWord
	 * @param bitSet
	 * @return
	 */
	private int hash(String hexWord, BitSet[] bitSet) {
		BitSet tmpSet = new BitSet();

		String bits = Long.toBinaryString(Long.parseLong(hexWord, 16));

		for (int j = 0; j < bits.length(); j++) {
			int bit = Integer.parseInt(bits.substring(j, j + 1));

			boolean bool = bit == 1;
			tmpSet.set(j, bool);
		}

		BitSet bitString = (BitSet) tmpSet.clone();

		int tot;
		BitSet resultSet = new BitSet();
		// hash that shit
		for (int i = 0; i < bitSet.length; i++) {

			tmpSet = (BitSet) bitString.clone();

			tot = 0;
			tmpSet.and(bitSet[i]);

			tot = tmpSet.cardinality() % 2;

			resultSet.set(i, tot == 1);

		}

		int index = (int) convertBitSetToLong(resultSet);

		if (!firstHashMade) {

			if (stringArr[index] == null) {
				stringArr[index] = new ArrayList<String>();
			}
			stringArr[index].add(hexWord);
		}

		return index;

	}

	public long convertBitSetToLong(BitSet bits) {
		long value = 0L;
		for (int i = 0; i < bits.length(); ++i) {
			value += bits.get(i) ? (1L << i) : 0L;
		}
		return value;
	}

	/**
	 * Read from hashMatrixFile in order to make the H matrix. Each hex word in hashMatrixFile was generated randomly.
	 * @throws IOException
	 */
	private void makeHashMatrix() throws IOException {
		
		FileOutputStream outputStream = null;
		FileInputStream inputStream = null;
		try {
			outputStream = new FileOutputStream(new File(outFile));
			inputStream = new FileInputStream(new File(hashMatrixFile));
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

		io = new Kattio(inputStream, outputStream);

		bitSet = new BitSet[22];

		String bits;
		String word;
		for (int i = 0; i < bitSet.length; i++) {

			bitSet[i] = new BitSet();

			word = io.getWord();
			io.println(word);

			bits = Long.toBinaryString(Long.parseLong(word, 16));

			for (int j = 0; j < bits.length(); j++) {
				int bit = Integer.parseInt(bits.substring(j, j + 1));

				boolean bool = bit == 1;
				bitSet[i].set(j, bool);
			}

		}

		io.close();
	}

	public static void main(String[] args) {
		try {
			new Main();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
